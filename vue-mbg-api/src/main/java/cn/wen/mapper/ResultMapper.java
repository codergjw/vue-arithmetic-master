package cn.wen.mapper;

import cn.wen.entity.Result;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Mapper
@Component
public interface ResultMapper extends BaseMapper<Result> {

}
