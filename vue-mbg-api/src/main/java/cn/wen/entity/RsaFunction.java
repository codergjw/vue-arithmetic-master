package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RsaFunction implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String rsaName;

    private String rsaType;

    private Integer rsaClass;

    private Date createTime;

    private Date updateTime;

    /**
     * 0表示失效，1表示失效
     */
    private Integer rsaStatus;

    private Integer rsaNumber;


}
