package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String subjectName;

    private String subjectType;

    private Integer subjectClass;

    private String subjectContext;

    private Date createTime;

    private Date updateTime;

    private Integer subjectStatus;

    private String subjectTag;

    private String subjectNumber;

    private String subjectExample;


}
