package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String username;

    private String nickname;

    private String password;

    private String phone;

    private String salt;

    private Date createTime;

    private Date updateTime;
    
    private Date loginTime;

    private String email;

    private String avatar;

    private Integer sex;

    private Boolean deleted;

    /**
     * 帐号启用状态：0->禁用；1->启用
     */
    private Integer status;


}
