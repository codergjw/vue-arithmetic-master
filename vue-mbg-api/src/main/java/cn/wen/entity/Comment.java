package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long solutionId;

    private Long discussId;

    private Integer parentId;

    private Integer toUid;

    private Integer level;

    private Long userId;

    private Date updateTime;

    private Date createTime;

    private Integer commentStatus;

    private Integer commentOwner;

    private Integer commentLike;

    private String commentContext;


}
