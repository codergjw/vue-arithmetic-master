package cn.wen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Solution implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String solutionTitle;

    private String solutionContext;

    private Date createTime;

    private Date updateTime;

    private Integer solutionStatus;

    private String solutionTag;

    private Integer solutionNumber;


}
