/*
 Navicat Premium Data Transfer

 Source Server         : 学生课表系统
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : db_rsa

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 26/03/2022 15:39:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NULL DEFAULT NULL,
  `subject_id` bigint(10) NULL DEFAULT NULL,
  `discuss_id` bigint(10) NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `update_date` datetime NULL DEFAULT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES (8, 5, NULL, 5, '2022-02-26 13:08:14', '2022-02-26 13:08:14', NULL);
INSERT INTO `collect` VALUES (51, 5, 1, NULL, '2022-03-21 15:40:42', '2022-03-21 15:40:42', NULL);
INSERT INTO `collect` VALUES (53, 5, NULL, 1, '2022-03-23 12:55:04', '2022-03-23 12:55:04', NULL);
INSERT INTO `collect` VALUES (54, 5, NULL, 4, '2022-03-23 13:34:05', '2022-03-23 13:34:05', NULL);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `solution_id` bigint(10) NULL DEFAULT NULL,
  `discuss_id` bigint(10) NULL DEFAULT NULL,
  `parent_id` int(10) NULL DEFAULT NULL,
  `to_uid` int(10) NULL DEFAULT NULL,
  `user_id` bigint(10) NULL DEFAULT NULL,
  `level` int(2) NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `comment_status` int(2) NULL DEFAULT NULL,
  `comment_owner` int(2) NULL DEFAULT NULL,
  `comment_like` int(2) NULL DEFAULT NULL,
  `comment_context` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (2, 1, NULL, NULL, NULL, 5, 1, '2022-02-25 00:29:48', '2022-02-25 00:29:50', 1, NULL, NULL, '我是solution第二条评论哦！！');
INSERT INTO `comment` VALUES (3, 1, NULL, 2, NULL, 6, 0, '2022-02-25 00:30:47', '2022-02-25 00:30:49', 1, NULL, NULL, '我是id为2的回复哦！');
INSERT INTO `comment` VALUES (4, 1, NULL, 3, NULL, 5, 0, '2022-02-25 08:34:53', '2022-02-25 00:31:59', 1, NULL, NULL, '我是id为3的回复哦！');
INSERT INTO `comment` VALUES (5, NULL, 1, NULL, NULL, 5, 1, '2022-02-26 14:51:33', '2022-02-26 14:51:35', 1, NULL, NULL, '我是评论讨论id为1 的评论');
INSERT INTO `comment` VALUES (6, NULL, 1, 5, NULL, 6, NULL, '2022-02-26 14:52:40', '2022-02-26 14:52:38', 0, NULL, NULL, '我是回复id为4的评论');
INSERT INTO `comment` VALUES (7, NULL, 1, NULL, NULL, 5, 1, '2022-02-26 15:01:10', '2022-02-26 15:01:12', 1, NULL, NULL, '我是评论讨论id为2 的第二条评论');
INSERT INTO `comment` VALUES (8, NULL, 1, 7, NULL, 6, NULL, '2022-02-26 15:01:55', '2022-02-26 15:01:58', 1, NULL, NULL, '我是回复id为7的评论');
INSERT INTO `comment` VALUES (9, NULL, 1, 6, NULL, 5, NULL, '2022-03-15 21:30:22', '2022-03-15 21:30:25', 1, NULL, NULL, '我是回复id为6的评论');
INSERT INTO `comment` VALUES (10, NULL, 1, 9, NULL, 6, NULL, '2022-03-15 21:31:07', '2022-03-15 21:31:09', 1, NULL, NULL, '我是回复id为9的评论');
INSERT INTO `comment` VALUES (11, 1, NULL, NULL, NULL, 6, 1, '2022-03-22 21:26:22', '2022-03-22 21:26:24', 1, NULL, NULL, '你个猪脑子');
INSERT INTO `comment` VALUES (19, NULL, 1, NULL, NULL, 5, 1, '2022-03-24 09:35:55', '2022-03-24 09:35:55', 1, NULL, NULL, '你好呀！！！！');
INSERT INTO `comment` VALUES (21, NULL, 1, 20, NULL, 5, NULL, '2022-03-24 09:37:07', '2022-03-24 09:37:07', 1, NULL, NULL, '好吧，我是猪脑子');
INSERT INTO `comment` VALUES (22, NULL, 1, 19, NULL, 5, NULL, '2022-03-24 16:09:15', '2022-03-24 16:09:15', 1, NULL, NULL, '猪脑子吗');

-- ----------------------------
-- Table structure for discuss
-- ----------------------------
DROP TABLE IF EXISTS `discuss`;
CREATE TABLE `discuss`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `discuss_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `discuss_context` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `discuss_status` int(2) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `discuss_tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `discuss_describe` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of discuss
-- ----------------------------
INSERT INTO `discuss` VALUES (1, '提问｜有关暑期实习的问题', '本人二本大三，想找个java后端方面的暑期实习。想问问一些问题。\n\n1.线上面试考算法是怎样一个形式？是说一下思路，还是要完整写出来？完整写出来的话，可不可以上网搜一些技巧？比如我打周赛时就会搜“如何去掉前缀0”，“如何实现字符串反转”等等这种可以直接调api的算法。\n\n2.实习就想在长沙周围，有没有就在长沙的中厂分厂的这些公司，我该怎么知道这些公司？\n\n3.实习时主要工资是多少，虽然实习不是很在意钱，但没达到平均总感觉心里超不爽的，暑期实习的平均工资是多少。\n\n4.暑期实习的项目经历，这个项目要做到什么地步才算好？要把springcloud那些注册中心，熔断降流什么的都用到吗，这些只是过了一遍，还没有实操经验，之前做的两个项目到前后端结合后速度明显变慢了，然后前端报错就做不下去了。\n\n5.中间件部分要求掌握到什么地步？我也只是过了一遍，跟着视频走也能做出来，到自己动手就无能为力了。\n\n就是这些啦，希望有大佬可以解解惑。', 1, '2022-02-21 01:13:07', '2022-02-21 01:13:10', '暑假实习', '本人二本大三，想找个java后端方面的暑期实习。想问问一些问题。\n\n1.线上面试考算法是怎样一个形式？是说一下思路，还是要完整写出来？完整写出来的话，可不可以上网搜一些技巧？比如我打周赛时就会搜“如何去掉前缀0”，“如何实现字符串反转”等等这种可以直接调api的算法。\n\n2.实习就想在长沙周围，有没有就在长沙的中厂分厂的这些公司，我该怎么知道这些公司？\n\n3.实习时主要工资是多少，虽然实习不是很在意钱，但没达到平均总感觉心里超不爽的，暑期实习的平均工资是多少。\n\n4.暑期实习的项目经历，这个项目要做到什么地步才算好？要把springcloud那些注册中心，熔断降流什么的都用到吗');
INSERT INTO `discuss` VALUES (4, '如何在「求职面试」中发布一篇帖子？', '如果你在同一个公司通过了其中一个环节后又收到了下一个环节的通知，请在原帖中更新而不是创建一个新的帖子。\n更好的排版可以让你的帖子更好地被看到，你可以点击「力扣编辑器使用说明」了解力扣编辑器的使用说明，点击「中文文案排版指北」\n如果你的帖子不符合以上发帖规定及「力扣社区管理规定」，力扣社区管理团队可能会对你的帖子&文章进行包括且不限于修改、删除等操作。\n关于「求职面试」发帖规则如果有更多疑问，可以「点击这里」向我们反馈并获得更多帮助～', 1, '2022-02-21 13:14:59', '2022-02-21 13:14:59', '暑假实习', '如果你在同一个公司通过了其中一个环节后又收到了下一个环节的通知，请在原帖中更新而不是创建一个新的帖子。\n更好的排版可以让你的帖子更好地被看到，你可以点击「力扣编辑器使用说明」了解力扣编辑器的使用说明，点击「中文文案排版指北」\n如果你的帖子不符合以上发帖规定及「力扣社区管理规定」，力扣社区管理团队可能会对你的帖子&文章进行包括且不限于修改、删除等操作。\n关于「求职面试」发帖规则如果有更多疑问，可以「点击这里」');
INSERT INTO `discuss` VALUES (5, '经验分享｜校招和面试的一些经验总结', '工作的话，尽量大厂。至于如何去百度阿里，我不知道……因为我也没进去……\n\n之前工作的公司，一个几十人，一个5000左右一个2000，一个被辞退，一个干了1年多，一个3天。\n\n个人评价：十几人的有十几人的不正规法，几千人的有几千人的不正规法。\n', 1, '2022-02-25 18:46:09', '2022-02-25 18:46:09', '校招', '工作的话，尽量大厂。至于如何去百度阿里，我不知道……因为我也没进去……\n\n之前工作的公司，一个几十人，一个5000左右一个2000，一个被辞退，一个干了1年多，一个3天。\n\n个人评价：十几人的有十几人的不正规法');
INSERT INTO `discuss` VALUES (8, '字节面经 | 面试题 | 跳动的“字节”', '**题目描述**\n> “字节”在数轴 0 点出发，向左或向右跳动（左右横跳），第一次跳 1 个单位，第二次 2 个单位，以此类推。给定系列目标 [x1, x2, ..., xn]，“字节”依次跳动到给定目标，并在到达后重置跳跃步长，继续按 1, 2,... 步长跳往下一目标。求“字节”跳动到 [x1, x2, ..., xn] 的最小步数 [s1, s2, ..., sn]\n\n**示例一**\n1. 输入： nums = [2, 1]\n2. 输出：3, 4\n\n**代码**\n\n```python\ndef jump_to(gap):\n    n = int((2 * gap) ** .5)\n    if n*(n + 1) < 2 * gap: # 定位上界\n        n += 1\n    if gap & 1: # 奇数情形\n        if n % 4 in (1, 2): return n\n        return n + (1 if n % 4 == 0 else 2)\n    if n % 4 in (0, 3): return n\n    return n + (1 if (n % 4) == 2 else 2)\n\ndef min_jump(nums):\n    gaps = [abs(nums[i+1] - nums[i]) for i in range(len(nums)-1)]\n    gaps.insert(0, abs(nums[0]))\n    totalstep, res = 0, []\n    for gap in gaps:\n        totalstep += jump_to(gap)\n        res.append(totalstep)\n    return res\n\n```\n\n\n', 1, '2022-03-23 12:29:38', '2022-03-23 12:29:38', 'python', '我介绍了一些有关字节跳动的面经');
INSERT INTO `discuss` VALUES (9, '校招 + 内推｜阿里云｜POLARDB 基础设施团队｜杭州 + 深圳', '### 为什么推荐来我们的团队呢\n**1.不加班**\n目前互联网行业整体的一个特点是什么呢，就是加班多，996。楼主之前在BAT的另外一家的时候，加班就相对比较多了。但是在我们目前的组，大家基本不加班，无996。\n\n**2.技术成长大**\n我们是做基础平台的，所以在日常的工作中，无论是研发流程质量，还是方案选型参考对比方面，都还是很规范的。在这样子的一个环境当中，对一个刚从学校出来的新人，还是非常友好的。可以比较快提升自己的技术。团队内的成员也都很唯实，大家都是奔着把事情做好的方面一起发力。团队内各种知乎大V，可以学习到很多，并且老大也很nice，很关注新人成长。\n\n**3.base地可选杭州或者深圳**\n如有兴趣，简历也可投递于：2689707540@qq.com', 1, '2022-03-23 13:20:48', '2022-03-23 13:20:48', '阿里巴巴', '楼主之前校招的时候也是经常逛 Leetcode。现在到了实习生内推的时间，一方面是帮助我们团队招聘实习生，另外一方面也是看看能否在能力范围内帮助大家解答一些疑惑，毕竟当初也是从 Leetcode 收获多多。');

-- ----------------------------
-- Table structure for discuss_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `discuss_user_relation`;
CREATE TABLE `discuss_user_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `discuss_id` bigint(10) NULL DEFAULT NULL,
  `user_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of discuss_user_relation
-- ----------------------------
INSERT INTO `discuss_user_relation` VALUES (1, 1, 5);
INSERT INTO `discuss_user_relation` VALUES (3, 4, 5);
INSERT INTO `discuss_user_relation` VALUES (4, 5, 5);
INSERT INTO `discuss_user_relation` VALUES (5, 7, 5);
INSERT INTO `discuss_user_relation` VALUES (6, 8, 5);
INSERT INTO `discuss_user_relation` VALUES (7, 9, 5);
INSERT INTO `discuss_user_relation` VALUES (8, 10, 5);

-- ----------------------------
-- Table structure for like_user_discuss_comment_subject_relation
-- ----------------------------
DROP TABLE IF EXISTS `like_user_discuss_comment_subject_relation`;
CREATE TABLE `like_user_discuss_comment_subject_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NULL DEFAULT NULL,
  `discuss_id` bigint(10) NULL DEFAULT NULL,
  `comment_id` bigint(10) NULL DEFAULT NULL,
  `subject_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of like_user_discuss_comment_subject_relation
-- ----------------------------
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (43, 5, 1, NULL, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (45, 5, NULL, 6, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (47, 5, NULL, 7, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (48, 5, NULL, 5, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (49, 5, NULL, 9, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (52, 5, NULL, 4, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (55, 5, NULL, 2, NULL);
INSERT INTO `like_user_discuss_comment_subject_relation` VALUES (58, 5, NULL, 11, NULL);

-- ----------------------------
-- Table structure for record
-- ----------------------------
DROP TABLE IF EXISTS `record`;
CREATE TABLE `record`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `record_input` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `record_output` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of record
-- ----------------------------

-- ----------------------------
-- Table structure for result
-- ----------------------------
DROP TABLE IF EXISTS `result`;
CREATE TABLE `result`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `result_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `result_context` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `result_status` int(2) NULL DEFAULT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of result
-- ----------------------------
INSERT INTO `result` VALUES (1, '哈希表', 'class Solution {\n    public int[] twoSum(int[] nums, int target) {\n        Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();\n        for (int i = 0; i < nums.length; ++i) {\n            if (hashtable.containsKey(target - nums[i])) {\n                return new int[]{hashtable.get(target - nums[i]), i};\n            }\n            hashtable.put(nums[i], i);\n        }\n        return new int[0];\n    }\n}\n\n', '2022-03-17 23:11:22', '2022-03-17 23:11:25', 1, NULL);

-- ----------------------------
-- Table structure for rsa_function
-- ----------------------------
DROP TABLE IF EXISTS `rsa_function`;
CREATE TABLE `rsa_function`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rsa_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rsa_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rsa_class` int(2) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `rsa_status` int(2) NULL DEFAULT NULL COMMENT '0表示失效，1表示失效',
  `rsa_number` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rsa_function
-- ----------------------------
INSERT INTO `rsa_function` VALUES (1, '行列式求值', '行列式', 2, '2022-02-23 19:05:45', '2022-02-23 19:05:47', 1, 100);
INSERT INTO `rsa_function` VALUES (3, '解线性方程组', '解方程组', 3, '2022-02-23 11:09:58', '2022-02-23 11:09:58', 1, NULL);
INSERT INTO `rsa_function` VALUES (4, '逆矩阵', '矩阵', 3, '2022-02-23 16:43:22', '2022-02-23 16:43:22', 1, NULL);

-- ----------------------------
-- Table structure for rsa_linear
-- ----------------------------
DROP TABLE IF EXISTS `rsa_linear`;
CREATE TABLE `rsa_linear`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `linear_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `linear_status` int(2) NULL DEFAULT NULL COMMENT '0表示失效，1表示失效',
  `linear_context` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rsa_linear
-- ----------------------------
INSERT INTO `rsa_linear` VALUES (1, '通过性质计算', '2022-02-23 19:07:51', '2022-03-11 16:05:17', 1, 'public class Main\n{\npublic static double main(double num [][],int n) {\n	if (n == 1) \n        return num[0][0];\n        double sum = 0;\n	for (int j = 0; j < n; j++) {\n		int pt = (n - 1) + j;\n		double[][] p1 = new double[n][n];\n		for (int row = 0; row < n; row++) {\n			for (int col = 0; col < n; col++) {\n				p1[row][col] = num[row][col];\n			}\n		}\n		for (int index = 0; index < n - 1; index++) {\n			for (int index1 = j; index1 < n - 1; index1++) {\n				p1[index][index1] = p1[index][index1 + 1];\n			}\n		}\n		double[][] temp = new double[n - 1][n - 1];\n		for (int row = 0; row < n - 1; row++) {\n			for (int col = 0; col < n - 1; col++) {\n				temp[row][col] = p1[row][col];\n			}\n		}\n		sum += num[n - 1][j] * Math.pow(-1, pt) * main(temp, n - 1);\n		}\n		return sum;\n	}\n}');
INSERT INTO `rsa_linear` VALUES (3, '代数余子式', '2022-02-23 16:18:35', '2022-02-23 16:18:35', 0, '	public static double GetLineTran(double[][] p, int n) {\n		if (n == 1) return p[0][0];\n		\n		double exChange = 1.0; // 记录行列式中交换的次数\n		boolean isZero = false; // 标记行列式某一行的最右边一个元素是否为零\n \n		for (int i = 0; i < n; i ++) {// i 表示行号\n			if (p[i][n - 1] != 0) { // 若第 i 行最右边的元素不为零\n				isZero = true;\n				\n				if (i != (n - 1)) { // 若第 i 行不是行列式的最后一行\n					for (int j = 0; j < n; j ++) { // 以此交换第 i 行与第 n-1 行各元素\n						double temp = p[i][j];\n						p[i][j] = p[n - 1][j];\n						p[n - 1][j] = temp;\n						\n						exChange *= -1.0;\n					}\n				}\n				\n				break;\n			}\n		}\n		\n		if (!isZero) return 0; // 行列式最右边一列元素都为零，则行列式为零。\n		\n		\n		for (int i = 0; i < (n - 1); i ++) {\n		// 用第 n-1 行的各元素，将第 i 行最右边元素 p[i][n-1] 变换为 0，\n		// 注意：i 从 0 到 n-2，第 n-1 行的最右边元素不用变换\n			if (p[i][n - 1] != 0) {\n				// 计算第  n-1 行将第 i 行最右边元素 p[i][n-1] 变换为 0的比例\n				double proportion = p[i][n - 1] / p[n - 1][n - 1];\n				\n				for (int j = 0; j < n; j ++) {\n					p[i][j] += p[n - 1][j] * (- proportion);\n				}\n			}\n		}\n		\n		return exChange * p[n - 1][n - 1] * GetLineTran(p, (n - 1));\n	}');
INSERT INTO `rsa_linear` VALUES (4, '变换', '2022-02-23 16:45:26', '2022-02-23 16:45:26', 1, 'public Matrix inverseMatrix() {\n    if (!this.isSquareMatrix()) {\n        System.out.println(\"不是方阵没有逆矩阵！\");\n        return null;\n    }\n    // 先在右边加上一个单位矩阵。\n    Matrix tempM = this.appendUnitMatrix();\n    // 再进行初等变换，把左边部分变成单位矩阵\n    double[][] tempData = tempM.getMatrixData();\n    int tempRow = tempData.length;\n    int tempCol = tempData[0].length;\n    // 对角线上数字为0时，用于交换的行号\n    int line = 0;\n    // 对角线上数字的大小\n    double bs = 0;\n    // 一个临时变量，用于交换数字时做中间结果用\n    double swap = 0;\n    for (int i = 0; i < tempRow; i++) {\n        // 将左边部分对角线上的数据等于0，与其他行进行交换\n        if (tempData[i][i] == 0) {\n            if (++line >= tempRow) {\n                System.out.println(\"此矩阵没有逆矩阵！\");\n                return null;\n            }\n \n            for (int j = 0; j < tempCol; j++) {\n                swap = tempData[i][j];\n                tempData[i][j] = tempData[line][j];\n                tempData[line][j] = swap;\n            }\n \n            // 当前行（第i行）与第line行进行交换后，需要重新对第i行进行处理\n            // 因此，需要将行标i减1，因为在for循环中会将i加1。\n            i--;\n            // 继续第i行处理，此时第i行的数据是原来第line行的数据。\n            continue;\n        }\n \n        // 将左边部分矩阵对角线上的数据变成1.0\n        if (tempData[i][i] != 1) {\n            bs = tempData[i][i];\n            for (int j = tempCol - 1; j >= 0; j--) {\n                tempData[i][j] /= bs;\n            }\n            // 将左边部分矩阵变成上对角矩阵，\n            // 所谓上对角矩阵是矩阵的左下角元素全为0\n            for (int iNow = i + 1; iNow < tempRow; iNow++) {\n                for (int j = tempCol - 1; j >= i; j--) {\n                    tempData[iNow][j] -= tempData[i][j] * tempData[iNow][i];\n                }\n            }\n        }\n    }\n \n    // 将左边部分矩阵从上对角矩阵变成单位矩阵，即将矩阵的右上角元素也变为0\n    for (int i = 0; i < tempRow - 1; i++) {\n        for (int iNow = i; iNow < tempRow - 1; iNow++) {\n            for (int j = tempCol - 1; j >= 0; j--) {\n                tempData[i][j] -= tempData[i][iNow + 1]\n                        * tempData[iNow + 1][j];\n            }\n        }\n    }\n \n    // 右边部分就是它的逆矩阵\n    Matrix c = null;\n    int cRow = tempRow;\n    int cColumn = tempCol / 2;\n    double[][] cData = new double[cRow][cColumn];\n    // 将右边部分的值赋给cData\n    for (int i = 0; i < cRow; i++) {\n        for (int j = 0; j < cColumn; j++) {\n            cData[i][j] = tempData[i][cColumn + j];\n        }\n    }\n    // 得到逆矩阵，返回\n    c = new Matrix(cData);\n    return c;\n}\n');

-- ----------------------------
-- Table structure for rsa_linear_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `rsa_linear_user_relation`;
CREATE TABLE `rsa_linear_user_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `rsa_id` bigint(10) NULL DEFAULT NULL,
  `linear_id` bigint(10) NULL DEFAULT NULL,
  `user_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rsa_linear_user_relation
-- ----------------------------
INSERT INTO `rsa_linear_user_relation` VALUES (1, 1, 1, 5);
INSERT INTO `rsa_linear_user_relation` VALUES (3, 1, 3, 5);
INSERT INTO `rsa_linear_user_relation` VALUES (4, 4, 4, 5);

-- ----------------------------
-- Table structure for rsa_record_relation
-- ----------------------------
DROP TABLE IF EXISTS `rsa_record_relation`;
CREATE TABLE `rsa_record_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `rsa_id` bigint(10) NULL DEFAULT NULL,
  `record_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rsa_record_relation
-- ----------------------------

-- ----------------------------
-- Table structure for solution
-- ----------------------------
DROP TABLE IF EXISTS `solution`;
CREATE TABLE `solution`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `solution_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `solution_context` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `solution_status` int(2) NULL DEFAULT NULL,
  `solution_tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `solution_number` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of solution
-- ----------------------------
INSERT INTO `solution` VALUES (1, '通过哈希表计算', '```java\nclass Solution {\n      public int[] twoSum(int[] nums, int target) {\n	  Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();\n	  for (int i = 0; i < nums.length; ++i) {\n	      if (hashtable.containsKey(target - nums[i])) {\n		  return new int[]{hashtable.get(target - nums[i]), i};\n	      }\n	      hashtable.put(nums[i], i);\n          }\n	  return new int[0];\n      }\n}\n\n```', '2022-02-22 02:28:32', '2022-03-25 06:25:43', 1, '哈希表', 0);
INSERT INTO `solution` VALUES (3, '用字典模拟哈希求解', '```python\ndef twoSum(nums, target):\n    hashmap={}\n    for ind,num in enumerate(nums):\n        hashmap[num] = ind\n    for i,num in enumerate(nums):\n        j = hashmap.get(target - num)\n        if j is not None and i!=j:\n            return [i,j]\n```\n\n', NULL, '2022-03-25 06:28:14', 0, 'Python', 0);
INSERT INTO `solution` VALUES (4, '两遍哈希表：', 'class Solution {\npublic:\n    vector<int> twoSum(vector<int>& nums, int target) {\n        map<int,int> a;//建立hash表存放数组元素\n        vector<int> b(2,-1);//存放结果\n        for(int i=0;i<nums.size();i++)\n            a.insert(map<int,int>::value_type(nums[i],i));\n        for(int i=0;i<nums.size();i++)\n        {\n            if(a.count(target-nums[i])>0&&(a[target-nums[i]]!=i))\n            //判断是否找到目标元素且目标元素不能是本身\n            {\n                b[0]=i;\n                b[1]=a[target-nums[i]];\n                break;\n            }\n        }\n        return b;\n    };\n};', NULL, NULL, 0, 'C++', 0);
INSERT INTO `solution` VALUES (6, '引入哈希表', 'class Solution {\n    public int[] twoSum(int[] nums, int target) {\n        Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();\n        for (int i = 0; i < nums.length; ++i) {\n            if (hashtable.containsKey(target - nums[i])) {\n                return new int[]{hashtable.get(target - nums[i]), i};\n            }\n            hashtable.put(nums[i], i);\n        }\n        return new int[0];\n    }\n}', NULL, '2022-02-22 14:30:26', 0, 'Java', 0);
INSERT INTO `solution` VALUES (7, '中心扩展算法', 'class Solution {\n    public String longestPalindrome(String s) {\n        if (s == null || s.length() < 1) {\n            return \"\";\n        }\n        int start = 0, end = 0;\n        for (int i = 0; i < s.length(); i++) {\n            int len1 = expandAroundCenter(s, i, i);\n            int len2 = expandAroundCenter(s, i, i + 1);\n            int len = Math.max(len1, len2);\n            if (len > end - start) {\n                start = i - (len - 1) / 2;\n                end = i + len / 2;\n            }\n        }\n        return s.substring(start, end + 1);\n    }\n\n    public int expandAroundCenter(String s, int left, int right) {\n        while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {\n            --left;\n            ++right;\n        }\n        return right - left - 1;\n    }\n}', NULL, '2022-02-22 14:31:33', 1, 'Java', 0);

-- ----------------------------
-- Table structure for subject
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `subject_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `subject_class` int(2) NULL DEFAULT NULL,
  `subject_context` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `subject_status` int(2) NULL DEFAULT NULL,
  `subject_tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `subject_example` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `subject_number` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject
-- ----------------------------
INSERT INTO `subject` VALUES (1, '两数之和', '数组', 1, '给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。\n\n你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。\n\n你可以按任意顺序返回答案。', '2022-02-19 23:01:51', '2022-02-19 23:01:54', 1, 'java', '输入：nums = [2,7,11,15], target = 9\n输出：[0,1]\n解释：因为 nums[0] + nums[1] !!输入：nums = [3,2,4], target = 6\r\n输出：[1,2]', 0);
INSERT INTO `subject` VALUES (2, '最长回文子串', '字符串', 2, '给你一个字符串 s，找到 s 中最长的回文子串。', '2022-02-19 23:13:15', '2022-02-19 23:13:18', 1, 'c', '输入：s = \"babad\"\r\n输出：\"bab\"\r\n解释：\"aba\" 同样是符合题意的答案。!!  输入：s = \"cbbd\"\r\n输出：\"bb\"', 0);

-- ----------------------------
-- Table structure for subject_result_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `subject_result_user_relation`;
CREATE TABLE `subject_result_user_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `subject_id` bigint(10) NULL DEFAULT NULL,
  `user_id` bigint(10) NULL DEFAULT NULL,
  `result_rsa_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_result_user_relation
-- ----------------------------
INSERT INTO `subject_result_user_relation` VALUES (2, 1, 5, 4);
INSERT INTO `subject_result_user_relation` VALUES (3, 1, 5, 5);
INSERT INTO `subject_result_user_relation` VALUES (4, 1, 5, 6);
INSERT INTO `subject_result_user_relation` VALUES (5, 1, 5, 7);
INSERT INTO `subject_result_user_relation` VALUES (6, 1, 5, 8);
INSERT INTO `subject_result_user_relation` VALUES (7, 1, 5, 9);
INSERT INTO `subject_result_user_relation` VALUES (8, 1, 5, 10);
INSERT INTO `subject_result_user_relation` VALUES (9, 1, 5, 11);
INSERT INTO `subject_result_user_relation` VALUES (10, 1, 5, 12);
INSERT INTO `subject_result_user_relation` VALUES (11, 1, 5, 13);
INSERT INTO `subject_result_user_relation` VALUES (12, 1, 5, 14);
INSERT INTO `subject_result_user_relation` VALUES (13, 1, 5, 15);
INSERT INTO `subject_result_user_relation` VALUES (14, 1, 5, 16);
INSERT INTO `subject_result_user_relation` VALUES (15, 1, 5, 17);
INSERT INTO `subject_result_user_relation` VALUES (16, 1, 5, 18);
INSERT INTO `subject_result_user_relation` VALUES (17, 1, 5, 19);
INSERT INTO `subject_result_user_relation` VALUES (18, 1, 5, 20);
INSERT INTO `subject_result_user_relation` VALUES (19, 1, 5, 21);
INSERT INTO `subject_result_user_relation` VALUES (20, 1, 5, 22);
INSERT INTO `subject_result_user_relation` VALUES (21, 1, 5, 23);
INSERT INTO `subject_result_user_relation` VALUES (22, 1, 5, 24);
INSERT INTO `subject_result_user_relation` VALUES (23, 1, 5, 25);
INSERT INTO `subject_result_user_relation` VALUES (24, 1, 5, 26);
INSERT INTO `subject_result_user_relation` VALUES (25, 1, 5, 27);
INSERT INTO `subject_result_user_relation` VALUES (26, 1, 5, 28);
INSERT INTO `subject_result_user_relation` VALUES (27, 1, 5, 29);
INSERT INTO `subject_result_user_relation` VALUES (28, 1, 5, 30);
INSERT INTO `subject_result_user_relation` VALUES (29, 1, 5, 31);
INSERT INTO `subject_result_user_relation` VALUES (30, 1, 5, 32);
INSERT INTO `subject_result_user_relation` VALUES (31, 1, 5, 33);
INSERT INTO `subject_result_user_relation` VALUES (32, 1, 5, 34);
INSERT INTO `subject_result_user_relation` VALUES (33, 1, 5, 35);
INSERT INTO `subject_result_user_relation` VALUES (34, 1, 5, 36);
INSERT INTO `subject_result_user_relation` VALUES (35, 1, 5, 37);
INSERT INTO `subject_result_user_relation` VALUES (36, 1, 5, 38);
INSERT INTO `subject_result_user_relation` VALUES (37, 1, 5, 39);
INSERT INTO `subject_result_user_relation` VALUES (38, 1, 5, 40);
INSERT INTO `subject_result_user_relation` VALUES (39, 1, 5, 41);
INSERT INTO `subject_result_user_relation` VALUES (40, 1, 5, 42);
INSERT INTO `subject_result_user_relation` VALUES (41, 1, 5, 43);
INSERT INTO `subject_result_user_relation` VALUES (42, 1, 5, 44);
INSERT INTO `subject_result_user_relation` VALUES (43, 1, 5, 45);
INSERT INTO `subject_result_user_relation` VALUES (44, 1, 5, 46);
INSERT INTO `subject_result_user_relation` VALUES (45, 1, 5, 47);
INSERT INTO `subject_result_user_relation` VALUES (46, 1, 5, 48);
INSERT INTO `subject_result_user_relation` VALUES (47, 1, 5, 49);
INSERT INTO `subject_result_user_relation` VALUES (48, 1, 5, 50);

-- ----------------------------
-- Table structure for subject_solution_user_relation
-- ----------------------------
DROP TABLE IF EXISTS `subject_solution_user_relation`;
CREATE TABLE `subject_solution_user_relation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject_id` bigint(20) NULL DEFAULT NULL,
  `solution_id` bigint(20) NULL DEFAULT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_solution_user_relation
-- ----------------------------
INSERT INTO `subject_solution_user_relation` VALUES (1, 1, 1, 5);
INSERT INTO `subject_solution_user_relation` VALUES (3, 1, 3, 5);
INSERT INTO `subject_solution_user_relation` VALUES (4, 1, 4, 5);
INSERT INTO `subject_solution_user_relation` VALUES (6, 1, 6, 5);
INSERT INTO `subject_solution_user_relation` VALUES (7, 2, 7, 5);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(6) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` int(2) NULL DEFAULT NULL,
  `deleted` bit(1) NULL DEFAULT NULL,
  `status` int(2) NULL DEFAULT 1,
  `login_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (5, '鸭梨', '丫丫', '64004ccfa58446bbe7398a5ca34e54c5', '19880030153', 'AcxOrb', '2022-02-19 06:33:34', '2022-02-19 06:33:34', '19880030153@qq.com', 'https://yaling.oss-cn-shenzhen.aliyuncs.com/20211121/4.jpg', 0, NULL, 1, '2022-03-25 06:46:24');
INSERT INTO `user` VALUES (6, '小飞飞', '飞飞', 'd63b2edbd7447c21e98fe5e5acf520f0', '15970239651', '+AFZrI', '2022-02-19 07:22:28', '2022-02-19 07:22:28', '932043654@qq.com', 'https://yaling.oss-cn-shenzhen.aliyuncs.com/20211121/4.jpg', 1, NULL, 1, '2022-02-19 09:53:54');

-- ----------------------------
-- Table structure for user_record_relation
-- ----------------------------
DROP TABLE IF EXISTS `user_record_relation`;
CREATE TABLE `user_record_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NULL DEFAULT NULL,
  `record_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_record_relation
-- ----------------------------

-- ----------------------------
-- Table structure for user_solution_relation
-- ----------------------------
DROP TABLE IF EXISTS `user_solution_relation`;
CREATE TABLE `user_solution_relation`  (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NULL DEFAULT NULL,
  `solution_id` bigint(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_solution_relation
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
