package cn.wen.controller;


import cn.wen.common.response.CommonResult;
import cn.wen.entity.Subject;
import cn.wen.entity.User;
import cn.wen.response.Result;
import cn.wen.service.SubjectService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  算法题库
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/admin/subject")
public class SubjectController {


    @Resource
    private SubjectService subjectService;

    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Map<String, Object> map = subjectService.list(keyword, pageSize, pageNum);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 修改信息
     */
    @ApiOperation("修改题目信息")
    @PostMapping("/update/{id}")
    public String updateSubject(@PathVariable Long id,@RequestBody Subject subject) {
        subject.setId(id);
        boolean result = subjectService.updateSubjectById(subject);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 是否启用
     */
    @ApiOperation("修改用户启用状态")
    @PostMapping("/update/status/{id}")
    public String updateStatus(@PathVariable Long id,@RequestParam(value = "status") Integer status) {
        Subject subject = new Subject();
        subject.setSubjectStatus(status);
        subject.setId(id);
        boolean result = subjectService.updateSubjectById(subject);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }


    @ApiOperation("删除指定用户信息")
    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = subjectService.delete(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

    /**
     * 注册功能
     */
    @PostMapping("/create")
    public String createDiscuss(@RequestBody Subject createSubject){
        // 查询算法存在该的账号，同时不能同绑定
        Subject subject = subjectService.getUserByUsername(createSubject.getSubjectName());
        if (subject != null){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("题目已存在！！"));
        }
        boolean resultSubject = subjectService.createSubject(createSubject);
        if (!resultSubject){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("创建失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("创建成功！！"));

    }


}
