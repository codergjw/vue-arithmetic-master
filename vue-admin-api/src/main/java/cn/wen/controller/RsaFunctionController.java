package cn.wen.controller;


import cn.wen.common.response.CommonResult;
import cn.wen.entity.RsaFunction;
import cn.wen.entity.Subject;
import cn.wen.response.Result;
import cn.wen.service.RsaFunctionService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/admin/rsa-function")
public class RsaFunctionController {


    @Resource
    private RsaFunctionService rsaFunctionService;


    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Map<String, Object> map = rsaFunctionService.list(keyword, pageSize, pageNum);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 修改信息
     */
    @ApiOperation("修改线算信息")
    @PostMapping("/update/{id}")
    public String updateRsaFunction(@PathVariable Long id,@RequestBody RsaFunction rsaFunction) {
        rsaFunction.setId(id);
        boolean result = rsaFunctionService.updateRsaFunctionById(rsaFunction);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 是否启用
     */
    @ApiOperation("修改启用状态")
    @PostMapping("/update/status/{id}")
    public String updateStatus(@PathVariable Long id,@RequestParam(value = "status") Integer status) {
        RsaFunction rsaFunction = new RsaFunction();
        rsaFunction.setRsaStatus(status);
        rsaFunction.setId(id);
        boolean result = rsaFunctionService.updateRsaFunctionById(rsaFunction);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }


    @ApiOperation("删除指定信息")
    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = rsaFunctionService.delete(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

    /**
     * 注册功能
     */
    @PostMapping("/create")
    public String createDiscuss(@RequestBody RsaFunction createRsaFunction){
        // 查询线算名称是否存在
        RsaFunction rsaFunction = rsaFunctionService.getUserByUsername(createRsaFunction.getRsaName());
        if (rsaFunction != null){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("题目已存在！！"));
        }
        boolean resultRsaFunction = rsaFunctionService.createRsaFunction(createRsaFunction);
        if (!resultRsaFunction){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("创建失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("创建成功！！"));

    }
}
