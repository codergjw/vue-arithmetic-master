package cn.wen.controller;


import cn.wen.common.response.CommonResult;
import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import cn.wen.response.Result;
import cn.wen.service.DiscussService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/admin/discuss")
public class DiscussController {

    @Resource
    private DiscussService discussService;

    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Map<String, Object> map = discussService.list(keyword, pageSize, pageNum);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 是否启用
     */
    @ApiOperation("修改讨论启用状态")
    @PostMapping("/update/status/{id}")
    public String updateStatus(@PathVariable Long id,@RequestParam(value = "status") Integer status) {
        Discuss discuss = new Discuss();
        discuss.setDiscussStatus(status);
        discuss.setId(id);
        boolean result = discussService.updateDiscuss(discuss);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 修改信息
     */
    @ApiOperation("修改讨论信息")
    @PostMapping("/update/{id}")
    public String updateDiscuss(@PathVariable Long id,@RequestBody Discuss discuss) {
        discuss.setId(id);
        boolean result = discussService.updateDiscuss(discuss);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    @ApiOperation("删除指定讨论信息")
    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = discussService.delete(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

    /**
     * 注册功能
     */
    @PostMapping("/create/{id}")
    @ApiOperation("添加讨论信息")
    public String discussCreate(@RequestBody Discuss createDiscuss,@PathVariable("id") Long userId){
        System.out.println(userId);
        boolean resultSubject = discussService.createDiscuss(createDiscuss,userId);
        if (!resultSubject){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("创建失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("创建成功！！"));

    }

}
