package cn.wen.controller;


import cn.wen.common.response.CommonResult;
import cn.wen.entity.User;
import cn.wen.response.Result;
import cn.wen.service.UserService;
import cn.wen.utils.MD5Util;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/admin/user")
public class UserController {

    @Resource
    private UserService userService;

    @Value("${user.avatar}")
    private String defaultAvatar;

    /**
     *
     * 获取用户信息
     */
    @ApiOperation("获取用户信息")
    @GetMapping("/info")
    public String getAdminInfo(@RequestParam String username){
        if(username == null || username.equals("")){
            return JSON.toJSONString(new Result().setCode(500).setMessage("信息获取失败！"));
        }
        User resultUser = userService.getUserByUsername(username);
        Map<Object, Object> map = new HashMap<>();
        map.put("username", resultUser.getUsername());
        map.put("icon", resultUser.getAvatar());
        return JSON.toJSONString(new Result().setCode(200).setMessage("登录成功").setData(map));
    }

    /**
     * 获取用户列表
     */
    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Map<String, Object> map = userService.list(keyword, pageSize, pageNum);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 修改信息
     */
    @ApiOperation("修改用户信息")
    @PostMapping("/update/{id}")
    public String updateAdmin(@PathVariable Long id,@RequestBody User user) {
        user.setId(id);
        boolean result = userService.updateUsrById(user);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 是否启用
     */
    @ApiOperation("修改用户启用状态")
    @PostMapping("/updateStatus/{id}")
    public String updateStatus(@PathVariable Long id,@RequestParam(value = "status") Integer status) {
        User umsAdmin = new User();
        umsAdmin.setStatus(status);
        umsAdmin.setId(id);
        boolean result = userService.updateUsrById(umsAdmin);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }


    @ApiOperation("删除指定用户信息")
    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = userService.delete(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

    /**
     * 注册功能
     */
    @PostMapping("/create")
    public String registerUser(@RequestBody User createUser){
        // 查询算法存在该邮箱的账号，同时邮箱不能同绑定
        User userByUserName = userService.getUserByUsername(createUser.getUsername());
        if (userByUserName != null){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("用户名已存在！！"));
        }
        User userByEmail = userService.getUserByEmail(createUser.getEmail());
        if (userByEmail != null){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("邮箱已存在！！"));
        }
        boolean resultUser = userService.createUser(createUser);
        if (!resultUser){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("创建失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("创建成功！！"));

    }


}
