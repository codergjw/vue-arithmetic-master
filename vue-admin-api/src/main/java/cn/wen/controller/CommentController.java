package cn.wen.controller;


import cn.wen.common.response.CommonResult;
import cn.wen.dao.CommentDao;
import cn.wen.dto.CommentParam;
import cn.wen.dto.SolutionParam;
import cn.wen.entity.Comment;
import cn.wen.entity.Solution;
import cn.wen.mapper.CommentMapper;
import cn.wen.response.Result;
import cn.wen.service.CommentService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/admin/comment")
public class CommentController {


    @Resource
    private CommentDao commentDao;

    @Resource
    private CommentService commentService;

    @Resource
    private CommentMapper commentMapper;

    /**
     * 获取楼主的评论
     *
     * @param keyword
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        Map<String, Object> map = commentService.listTop(keyword, pageSize, pageNum);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 获取非楼主的评论
     *
     * @param keyword
     * @param pageSize
     * @param pageNum
     * @return
     */
    @GetMapping("/child/list")
    public String listCommentChild(@RequestParam(value = "keyword", required = false) String keyword,
                                   @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                   @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "commentId", required = false) Long commentId) {
        Map<String, Object> map = commentService.listCommentChild(commentId, keyword, pageSize, pageNum);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 是否启用
     */
    @ApiOperation("修改评论启用状态")
    @PostMapping("/update/status/{id}")
    public String updateStatus(@PathVariable Long id,
                               @RequestParam(value = "status") Integer status) {
        Comment comment = new Comment();
        comment.setId(id);
        comment.setCommentStatus(status);
        boolean result = commentService.updateComment(comment);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 修改信息
     */
    @ApiOperation("修改评论信息")
    @PostMapping("/update/{id}")
    public String updateSolution(@PathVariable("id") Long id,
                                 @RequestBody Comment comment) {
        comment.setId(id);
        comment.setUpdateTime(new Date());
        boolean result = commentService.updateComment(comment);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    @ApiOperation("删除指定评论信息")
    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = commentService.delete(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }
}
