package cn.wen.controller;


import cn.wen.common.response.CommonResult;
import cn.wen.dao.SolutionDao;
import cn.wen.dto.SolutionParam;
import cn.wen.entity.Discuss;
import cn.wen.entity.Solution;
import cn.wen.mapper.SolutionMapper;
import cn.wen.response.Result;
import cn.wen.service.SolutionService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/admin/solution")
public class SolutionController {


    @Resource
    private SolutionService solutionService;

    @Resource
    private SolutionDao solutionDao;

    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                       @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                       @RequestParam(value = "subjectId", required = true) Long subjectId) {
        Map<String, Object> map = solutionService.list(keyword, pageSize, pageNum,subjectId);
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 是否启用
     */
    @ApiOperation("修改题解启用状态")
    @PostMapping("/update/status/{id}")
    public String updateStatus(@PathVariable Long id,
                               @RequestParam(value = "status") Integer status,
                               @RequestParam(value = "subjectId") Long subjectId) {
        boolean result = solutionService.updateStatus(id,subjectId,status);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 修改信息
     */
    @ApiOperation("修改题解信息")
    @PostMapping("/update/{id}/{subjectId}")
    public String updateSolution(@PathVariable("id") Long id,
                                 @RequestBody Solution solution,
                                 @PathVariable("subjectId") Long subjectId) {
        solution.setId(id);
        solution.setUpdateTime(new Date());
        if (solution.getSolutionStatus() == 1){
            boolean b = solutionDao.updateStatusIfAll(subjectId,0);
        }
        boolean result = solutionService.updateSolution(solution);
        if (result) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    @ApiOperation("删除指定题解信息")
    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = solutionService.delete(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

    /**
     * 注册功能
     */
    @PostMapping("/create")
    @ApiOperation("添加题解信息")
    public String discussCreate(@RequestBody SolutionParam solutionParam){
        boolean resultSubject = solutionService.createSolution(solutionParam,solutionParam.getUserId());
        if (!resultSubject){
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("创建失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("创建成功！！"));

    }

}
