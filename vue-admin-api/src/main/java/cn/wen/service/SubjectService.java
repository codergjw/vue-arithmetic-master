package cn.wen.service;

import cn.wen.entity.Subject;
import cn.wen.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface SubjectService extends IService<Subject> {

    /**
     * 通过名称获取题目信息
     */
    Subject getUserByUsername(String subjectName);

    /**
     * 获取题目列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 修改题目信息
     */
    boolean updateSubjectById(Subject subject);

    /**
     * 注册题目
     */
    boolean createSubject(Subject subject);

    /**
     * 删除题目
     */
    boolean delete(Long id);
}
