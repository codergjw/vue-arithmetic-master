package cn.wen.service;

import cn.wen.dto.RsaLinearParam;
import cn.wen.dto.SolutionParam;
import cn.wen.entity.RsaLinear;
import cn.wen.entity.Solution;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface RsaLinearService extends IService<RsaLinear> {

    /**
     * 获取算法列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum, Long rsaId);

    /**
     * 修改rsaLinear数据
     * @param rsaLinear
     * @return
     */
    boolean updateRsaLinear(RsaLinear rsaLinear);

    /**
     * 增加算法
     */
    boolean createRsaLinear(RsaLinearParam rsaLinearParam, Long userId);

    /**
     * 删除算法
     */
    boolean delete(Long id);

    /**
     * 修改状态
     */
    boolean updateStatus(Long linearId,Long rsaId,Integer status);

}
