package cn.wen.service;

import cn.wen.dto.CommentParam;
import cn.wen.entity.Comment;
import cn.wen.entity.Discuss;
import java.util.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface CommentService extends IService<Comment> {


    /**
     * 获取头评论列表
     */
    Map<String, Object> listTop(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 获取评论列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 获取非楼主评论列表
     */
    Map<String, Object> listCommentChild(Long commentId,String keyword, Integer pageSize, Integer pageNum);

    /**
     * 修改comment数据
     * @param comment
     * @return
     */
    boolean updateComment(Comment comment);


    /**
     * 删除评论
     */
    boolean delete(Long id);

}
