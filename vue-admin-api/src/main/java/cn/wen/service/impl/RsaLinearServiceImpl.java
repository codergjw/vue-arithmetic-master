package cn.wen.service.impl;

import cn.wen.dao.RsaLinearDao;
import cn.wen.dto.RsaLinearParam;
import cn.wen.dto.SolutionParam;
import cn.wen.entity.RsaLinear;
import cn.wen.mapper.RsaLinearMapper;
import cn.wen.service.RsaLinearService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class RsaLinearServiceImpl extends ServiceImpl<RsaLinearMapper, RsaLinear> implements RsaLinearService {

    @Resource
    private RsaLinearDao rsaLinearDao;

    @Resource
    private RsaLinearMapper rsaLinearMapper;

    @Override
    public Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum, Long rsaId) {
        List<SolutionParam> list = rsaLinearDao.rsaLinearList(keyword, rsaId);
        PageInfo pageInfo = new PageInfo(list);
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public boolean updateRsaLinear(RsaLinear rsaLinear) {
        return rsaLinearDao.updateRsaLinear(rsaLinear);
    }

    @Override
    public boolean createRsaLinear(RsaLinearParam rsaLinearParam, Long userId) {
        RsaLinear rsaLinear = new RsaLinear();
        BeanUtils.copyProperties(rsaLinearParam,rsaLinear);
        System.out.println(rsaLinearParam);
        if (rsaLinear.getLinearStatus() == 1){
            rsaLinearDao.updateStatusIfAll(rsaLinearParam.getRsaId(),0);
        }
        rsaLinear.setCreateTime(new Date());
        rsaLinear.setUpdateTime(new Date());
        int insert = rsaLinearMapper.insert(rsaLinear);
        if (insert <0){
            return false;
        }
        boolean result = rsaLinearDao.insertUserRsaFunctionRsaLinearRelation(userId, rsaLinear.getId(), rsaLinearParam.getRsaId());
        return result;
    }

    @Override
    public boolean delete(Long id) {
        int i = rsaLinearMapper.deleteById(id);
        if (i <0) return false;
        return rsaLinearDao.deleteUserRsaFunctionRsaLinearRelation(id);
    }

    @Override
    public boolean updateStatus(Long linearId, Long rsaId, Integer status) {
        /**
         * 先找到所以的 rsa的数据修改
         */
        if (status == 1){
            rsaLinearDao.updateStatusIfAll(rsaId,0);
        }
        RsaLinear rsaLinear = new RsaLinear();
        rsaLinear.setLinearStatus(status);
        rsaLinear.setId(linearId);
        return updateRsaLinear(rsaLinear);
    }
}
