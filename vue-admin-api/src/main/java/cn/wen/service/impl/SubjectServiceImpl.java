package cn.wen.service.impl;

import cn.wen.dao.SubjectDao;
import cn.wen.entity.Subject;
import cn.wen.entity.User;
import cn.wen.mapper.SubjectMapper;
import cn.wen.service.SubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements SubjectService {

    @Resource
    private SubjectMapper subjectMapper;

    @Resource
    private SubjectDao subjectDao;

    @Override
    public Subject getUserByUsername(String subjectName) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("subject_name",subjectName);
        return subjectMapper.selectOne(queryWrapper);
    }

    @Override
    public Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum) {
        QueryWrapper queryWrapper = new QueryWrapper();
        PageHelper.startPage(pageNum,pageSize);
        if(!StringUtils.isEmpty(keyword)){
            queryWrapper.like("subject_name",keyword);
        }
        List<Subject> list = subjectMapper.selectList(queryWrapper);
        PageInfo pageInfo = new PageInfo(list);
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public boolean updateSubjectById(Subject subject) {
        return subjectDao.updateSubjectById(subject);
    }

    @Override
    public boolean createSubject(Subject subject) {
        subject.setCreateTime(new Date());
        subject.setUpdateTime(new Date());
        subject.setSubjectStatus(1);
        System.out.println(subject);
        return subjectMapper.insert(subject) >0;
    }

    @Override
    public boolean delete(Long id) {
        return subjectMapper.deleteById(id) >0;
    }
}
