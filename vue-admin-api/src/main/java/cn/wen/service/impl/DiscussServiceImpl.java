package cn.wen.service.impl;

import cn.wen.dao.DiscussDao;
import cn.wen.dto.DiscussParam;
import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import cn.wen.entity.User;
import cn.wen.mapper.DiscussMapper;
import cn.wen.service.DiscussService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class DiscussServiceImpl extends ServiceImpl<DiscussMapper, Discuss> implements DiscussService {

    @Resource
    private DiscussMapper discussMapper;

    @Resource
    private DiscussDao discussDao;

    @Override
    public Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum) {
        List<DiscussParam> list = discussDao.discussList(keyword);
        PageInfo pageInfo = new PageInfo(list);
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public boolean updateDiscuss(Discuss discuss) {
        return discussDao.updateDiscuss(discuss);
    }

    @Override
    public boolean createDiscuss(Discuss discuss,Long userId) {
        discuss.setCreateTime(new Date());
        discuss.setUpdateTime(new Date());
        int insert = discussMapper.insert(discuss);
        if (insert < 0){
            return false;
        }
        return discussDao.insertUserDiscussRelation(userId,discuss.getId());
    }

    @Override
    public boolean delete(Long id) {
        Long userId = discussDao.getUserIdByDiscussId(id);
        boolean b = discussDao.deleteUserDiscussRelation(userId, id);
        if (!b){
            return false;
        }
        return discussMapper.deleteById(id) >0;
    }


}
