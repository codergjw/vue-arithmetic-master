package cn.wen.service.impl;

import cn.wen.dao.RsaLinearDao;
import cn.wen.dao.RsaFunctionDao;
import cn.wen.entity.RsaFunction;
import cn.wen.mapper.RsaFunctionMapper;
import cn.wen.service.RsaFunctionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class RsaFunctionServiceImpl extends ServiceImpl<RsaFunctionMapper, RsaFunction> implements RsaFunctionService {

    @Resource
    private RsaFunctionDao rsaFunctionDao;

    @Resource
    private RsaFunctionMapper rsaFunctionMapper;

    @Resource
    private RsaLinearDao rsaLinearDao;

    @Override
    public RsaFunction getUserByUsername(String rsaName) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("rsa_name",rsaName);
        return rsaFunctionMapper.selectOne(queryWrapper);
    }

    @Override
    public Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum) {
        QueryWrapper queryWrapper = new QueryWrapper();
        PageHelper.startPage(pageNum,pageSize);
        if(!StringUtils.isEmpty(keyword)){
            queryWrapper.like("rsa_name",keyword);
        }
        List<RsaFunction> list = rsaFunctionMapper.selectList(queryWrapper);
        PageInfo pageInfo = new PageInfo(list);
        Map<String, Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public boolean updateRsaFunctionById(RsaFunction rsaFunction) {
        return rsaFunctionDao.updateRsaFunctionById(rsaFunction);
    }

    @Override
    public boolean createRsaFunction(RsaFunction rsaFunction) {
        rsaFunction.setCreateTime(new Date());
        rsaFunction.setUpdateTime(new Date());
        return rsaFunctionMapper.insert(rsaFunction) >0;
    }

    @Override
    public boolean delete(Long rsaId) {
        // 删除 linear有关rsa的数据
        rsaLinearDao.deleteRsaLinearByRsaId(rsaId);
        // 删除 关系表
        rsaFunctionDao.deleteRsaLinearUserRelation(rsaId);
        // 删除 rsa表的数据
        int deleteById = rsaFunctionMapper.deleteById(rsaId);
        return deleteById >0;
    }
}
