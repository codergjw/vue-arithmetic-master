package cn.wen.service.impl;

import cn.wen.entity.Result;
import cn.wen.mapper.ResultMapper;
import cn.wen.service.ResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class ResultServiceImpl extends ServiceImpl<ResultMapper, Result> implements ResultService {

}
