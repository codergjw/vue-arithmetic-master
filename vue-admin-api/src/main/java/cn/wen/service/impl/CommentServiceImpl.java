package cn.wen.service.impl;

import cn.wen.dao.CommentDao;
import cn.wen.dto.CommentParam;
import cn.wen.entity.Comment;
import cn.wen.mapper.CommentMapper;
import cn.wen.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Resource
    private CommentMapper commentMapper;

    @Resource
    private CommentDao commentDao;

    @Override
    public Map<String, Object> listTop(String keyword, Integer pageSize, Integer pageNum) {
        List<CommentParam> commentList = commentDao.commentList(keyword);
        PageInfo pageInfo = new PageInfo(commentList);
        Map<String, Object> map = new HashMap<>();
        map.put("list", commentList);
        map.put("total", pageInfo.getTotal());
        return map;

    }

    @Override
    public Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum) {
        List<CommentParam> commentList = commentDao.commentList(keyword);
        //获取二级评论
        for (CommentParam commentParam : commentList) {
            commentParam.setReplyComments(commentDao.listTreeComment(commentParam.getId(),keyword));
        }
        List<CommentParam> commentParams = eachComment(commentList);
        PageInfo pageInfo = new PageInfo(commentParams);
        Map<String, Object> map = new HashMap<>();
        map.put("list",commentParams);
        map.put("total",pageInfo.getTotal());
        return map;
    }

    /**
     * 循环每个顶级的评论节点
     *
     * @param comments
     * @return
     */
    private List<CommentParam> eachComment(List<CommentParam> comments) {
        List<CommentParam> commentsView = new ArrayList<>();
        for (CommentParam comment : comments) {
            CommentParam c = new CommentParam();
            BeanUtils.copyProperties(comment, c);
            commentsView.add(c);
        }
        //合并评论的各层子代到第一级子代集合中
        combineChildren(commentsView);
        return commentsView;
    }

    /**
     * @param comments root根节点，blog不为空的对象集合
     * @return
     */
    private void combineChildren(List<CommentParam> comments) {

        for (CommentParam comment : comments) {
            List<CommentParam> replys1 = comment.getReplyComments();
            for (CommentParam reply1 : replys1) {
                //循环迭代，找出子代，存放在tempReplys中
                recursively(reply1);
            }
            //修改顶级节点的reply集合为迭代处理后的集合
            comment.setReplyComments(tempReplys);
            //清除临时存放区
            tempReplys = new ArrayList<>();
        }
    }

    //存放迭代找出的所有子代的集合
    private List<CommentParam> tempReplys = new ArrayList<>();

    /**
     * 递归迭代，剥洋葱
     *
     * @param comment 被迭代的对象
     * @return
     */
    private void recursively(CommentParam comment) {
        tempReplys.add(comment);//顶节点添加到临时存放集合
        if (comment.getReplyComments().size() > 0) {
            List<CommentParam> replys = comment.getReplyComments();
            for (CommentParam reply : replys) {
                CommentParam commentParam = new CommentParam();
                BeanUtils.copyProperties(reply,commentParam);
                tempReplys.add(commentParam);
                if (reply.getReplyComments().size() > 0) {
                    recursively(reply);
                }
            }
        }
    }


    @Override
    public Map<String, Object> listCommentChild(Long commentId,String keyword, Integer pageSize, Integer pageNum) {
        List<CommentParam> commentChildParamList = commentDao.commentByIdList(keyword,commentId);
        //获取二级评论
        for (CommentParam commentParam : commentChildParamList) {
            commentParam.setReplyComments(commentDao.listTreeComment(commentParam.getId(),keyword));
        }
        List<CommentParam> commentParams = eachComment(commentChildParamList);
        List<CommentParam> replyComments = commentParams.get(0).getReplyComments();
        PageInfo pageInfo = new PageInfo(replyComments);
        Map<String, Object> map = new HashMap<>();
        map.put("list",replyComments);
        map.put("total",pageInfo.getTotal());
        return map;
    }

    @Override
    public boolean updateComment(Comment comment) {
        return commentDao.updateComment(comment);
    }

    @Override
    public boolean delete(Long id) {
        return commentMapper.deleteById(id) >0;
    }
}
