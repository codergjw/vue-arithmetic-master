package cn.wen.service;

import cn.wen.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.*;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public interface UserService extends IService<User> {

    /**
     * 通过id 获取用户信息
     */
    User getUserById(Long id);

    /**
     * 通过用户名获取用户信息
     */
    User getUserByUsername(String username);

    /**
     * 通过邮箱查询用户
     */
    User getUserByEmail(String email);


    /**
     * 获取用户列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum);


    String getPasswordById(Long id);

    /**
     * 修改用户信息
     */
    boolean updateUsrById(User user);

    /**
     * 注册用户
     */
    boolean createUser(User user);

    /**
     * 删除用户
     */
    boolean delete(Long id);

}
