package cn.wen.service;

import cn.wen.entity.RsaFunction;
import cn.wen.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface RsaFunctionService extends IService<RsaFunction> {

    /**
     * 通过名称获取线算信息
     */
    RsaFunction getUserByUsername(String rsaName);

    /**
     * 获取线算列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 修改线算信息
     */
    boolean updateRsaFunctionById(RsaFunction rsaFunction);

    /**
     * 注册线算
     */
    boolean createRsaFunction(RsaFunction rsaFunction);

    /**
     * 删除线算
     */
    boolean delete(Long rsaId);
}
