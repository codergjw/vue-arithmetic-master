package cn.wen.service;

import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface DiscussService extends IService<Discuss> {


    /**
     * 获取讨论列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 修改discuss数据
     * @param discuss
     * @return
     */
    boolean updateDiscuss(Discuss discuss);

    /**
     * 增加讨论
     */
    boolean createDiscuss(Discuss discuss,Long userId);

    /**
     * 删除讨论
     */
    boolean delete(Long id);

}
