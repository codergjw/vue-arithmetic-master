package cn.wen.service;

import cn.wen.dto.SolutionParam;
import cn.wen.entity.Discuss;
import cn.wen.entity.Solution;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface SolutionService extends IService<Solution> {

    /**
     * 获取讨论列表
     */
    Map<String, Object> list(String keyword, Integer pageSize, Integer pageNum,Long subjectId);

    /**
     * 修改solution数据
     * @param solution
     * @return
     */
    boolean updateSolution(Solution solution);

    /**
     * 增加题解
     */
    boolean createSolution(SolutionParam solutionParam, Long userId);

    /**
     * 删除题解
     */
    boolean delete(Long id);

    /**
     * 修改状态
     */
    boolean updateStatus(Long solutionId,Long subjectId,Integer status);
}
