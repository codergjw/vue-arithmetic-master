package cn.wen.dto;

import cn.wen.entity.User;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DiscussFrontParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String discussTitle;

    private String discussContext;

    private String discussDescribe;

    private Integer discussStatus;

    private String discussTag;

    private Date createTime;

    private Date updateTime;

    private String belongUsername;

    private String userAvatar;

    private Long myUp;

    private Long myCollect;

    private int likeNumber;

    private int commentNumber;

    private int collectNumber;

    private User discussUser;
}
