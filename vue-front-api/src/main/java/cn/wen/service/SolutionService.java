package cn.wen.service;

import cn.wen.dto.SolutionParam;
import cn.wen.dto.SubjectParam;
import cn.wen.entity.Comment;
import cn.wen.entity.Solution;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface SolutionService extends IService<Solution> {

    /**
     * 通过id获取题目详情
     */
    SolutionParam getSolutionById(Long solutionId, Long userId);

    // 获取激活的solution的
    SolutionParam getActiveSolutionIdById(Long subjectId);

    // solution的页面添加评论
    boolean insertCommentSolution(Long solutionId,Long userId, String commentText);

}
