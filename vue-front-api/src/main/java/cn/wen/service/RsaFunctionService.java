package cn.wen.service;

import cn.wen.entity.RsaFunction;
import cn.wen.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface RsaFunctionService extends IService<RsaFunction> {

    /**
     * 获取题目列表
     */
    List<RsaFunction> rsaFunctionList(String keyword);


    RsaFunction getRsaFunctionById(Long rsaId);

    /**
     * 解法
     */
    double solution(String data,Long rsaId);


}
