package cn.wen.service;

import cn.wen.entity.Result;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface ResultService extends IService<Result> {

    boolean insertResult(Long userId, Long subjectId,String resultContext);

}
