package cn.wen.service;

import cn.wen.dto.SubjectParam;
import cn.wen.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface SubjectService extends IService<Subject> {


    /**
     * 通过名称获取题目信息
     */
    Subject getUserByUsername(String subjectName);

    /**
     * 获取题目列表
     */
    List<Subject> subjectList(String keyword,int subjectClass);

    /**
     * 通过id获取题目详情
     */
    SubjectParam getSubjectById(Long subjectId, Long userId);

}
