package cn.wen.service;

import cn.wen.dto.CommentParam;
import cn.wen.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface CommentService extends IService<Comment> {


    /**
     * 获取评论列表
     */
    List<CommentParam> list(Long discussId,Long userId);


    /**
     * 获取评论列表
     */
    List<CommentParam> listSolutionComment(Long solutionId,Long userId);


    /**
     * 修改comment数据
     * @param comment
     * @return
     */
    boolean updateComment(Comment comment);


    /**
     * 删除评论
     */
    boolean deleteComment(Long id);

}
