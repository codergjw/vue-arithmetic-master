package cn.wen.service;

import cn.wen.entity.Collect;
import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface CollectService extends IService<Collect> {

    /**
     * 通过userId 获取subject收藏列表
     */
    List<Subject> subjectCollectList(Long userId);

    /**
     * 通过userId 获取discuss收藏列表
     */
    List<Discuss> discussCollectList(Long userId);

    /**
     * 通过 collectId 删除
     */
    boolean deleteBySubjectId(Long id,Long userId);

     /**
     * 删除题目
     */
    boolean deleteByDiscussId(Long id,Long userId);

    /**
     * 删除讨论点赞
     */
    boolean deleteByDiscussLikeId(Long id,Long userId);

    /**
     * 增加讨论点赞
     */
    boolean insertByDiscussLikeId(Long id,Long userId);

    /**
     * 增加收藏
     */
    boolean insertByDiscussCollectId(Long id,Long userId);
}
