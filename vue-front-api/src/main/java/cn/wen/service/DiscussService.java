package cn.wen.service;

import cn.wen.dto.DiscussFrontParam;
import cn.wen.entity.Discuss;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
public interface DiscussService extends IService<Discuss> {


    /**
     * 获取讨论列表
     */
    List<DiscussFrontParam> list(Integer sort, Long userId);

    /**
     * 增加讨论
     */
    boolean createDiscuss(Discuss discuss,Long userId);

    DiscussFrontParam getDiscussById(Long userId,Long discussId);

    /**
     * 获取标签相同的
     */
    List<DiscussFrontParam> list(String discussTag, String discussTitle);

    // 插入回复
    boolean insertReplyDiscuss(Long discussId,Long userId, String replyContext);

    // solution的页面回复评论
    boolean insertReplyCommentSolution(Long discussId,Integer parentId,Long userId, String commentText);
}
