package cn.wen.service.impl;

import cn.wen.dao.SubjectDao;
import cn.wen.dto.SubjectParam;
import cn.wen.entity.Subject;
import cn.wen.mapper.SubjectMapper;
import cn.wen.service.SubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements SubjectService {

    @Resource
    private SubjectMapper subjectMapper;

    @Resource
    private SubjectDao subjectDao;

    @Override
    public Subject getUserByUsername(String subjectName) {
        return null;
    }

    @Override
    public List<Subject> subjectList(String keyword,int subjectClass) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (!"".equals(keyword) && keyword != null){
            queryWrapper.like("subject_name",keyword);
        }
        if (subjectClass >0){
            queryWrapper.eq("subject_class",subjectClass);
        }
        return subjectMapper.selectList(queryWrapper);
    }

    @Override
    public SubjectParam getSubjectById(Long subjectId,Long userId) {
        SubjectParam subjectParam = new SubjectParam();
        Subject subject = subjectMapper.selectById(subjectId);
        BeanUtils.copyProperties(subject,subjectParam);
        subjectParam.setMyCollect(subjectDao.getIsCollectById(userId, subjectId) == null ? 0 : subjectDao.getIsCollectById(userId, subjectId));
        subjectParam.setMyUp(subjectDao.getIsLikeById(userId, subjectId) == null ? 0 : subjectDao.getIsLikeById(userId, subjectId));
        subjectParam.setCollectNumber(subjectDao.getCollectNumberById(subjectId));
        subjectParam.setLikeNumber(subjectDao.getLikeNumberById(subjectId));
        return subjectParam;
    }

}
