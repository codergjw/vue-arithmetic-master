package cn.wen.service.impl;

import cn.wen.dao.DiscussDao;
import cn.wen.dto.DiscussFrontParam;
import cn.wen.entity.Comment;
import cn.wen.entity.Discuss;
import cn.wen.mapper.CommentMapper;
import cn.wen.mapper.DiscussMapper;
import cn.wen.service.DiscussService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class DiscussServiceImpl extends ServiceImpl<DiscussMapper, Discuss> implements DiscussService {

    @Resource
    private DiscussDao discussDao;


    @Resource
    private DiscussMapper discussMapper;

    @Resource
    private CommentMapper commentMapper;

    @Override
    public List<DiscussFrontParam> list(Integer Sort, Long userId) {
        List<DiscussFrontParam> discussFrontParams = discussDao.discussList(Sort, userId);
        for (DiscussFrontParam discussFrontParam : discussFrontParams) {
            discussFrontParam.setMyCollect(discussDao.getIsCollectById(userId, discussFrontParam.getId()) == null ? 0 : discussDao.getIsCollectById(userId, discussFrontParam.getId()));
            discussFrontParam.setMyUp(discussDao.getIsLikeById(userId, discussFrontParam.getId()) == null ? 0 : discussDao.getIsLikeById(userId, discussFrontParam.getId()));
            discussFrontParam.setCollectNumber(discussDao.getCollectNumberById(discussFrontParam.getId()));
            discussFrontParam.setLikeNumber(discussDao.getLikeNumberById(discussFrontParam.getId()));
        }
        return discussFrontParams;
    }

    @Override
    public boolean createDiscuss(Discuss discuss, Long userId) {
        discuss.setCreateTime(new Date());
        discuss.setUpdateTime(new Date());
        discuss.setDiscussStatus(1);
        int insert = discussMapper.insert(discuss);
        if (insert < 0) {
            return false;
        }
        return discussDao.insertUserDiscussRelation(userId, discuss.getId());
    }

    @Override
    public DiscussFrontParam getDiscussById(Long userId, Long discussId) {
        DiscussFrontParam discussById = discussDao.getDiscussById(discussId);
        discussById.setMyCollect(discussDao.getIsCollectById(userId, discussId) == null ? 0 : discussDao.getIsCollectById(userId, discussId));
        discussById.setMyUp(discussDao.getIsLikeById(userId, discussId) == null ? 0 : discussDao.getIsLikeById(userId, discussId));
        discussById.setCollectNumber(discussDao.getCollectNumberById(discussId));
        discussById.setLikeNumber(discussDao.getLikeNumberById(discussId));
        return discussById;
    }

    @Override
    public List<DiscussFrontParam> list(String discussTag, String discussTitle) {
        return discussDao.getRelatedList(discussTag);
    }

    @Override
    public boolean insertReplyDiscuss(Long discussId, Long userId, String replyContext) {
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setUpdateTime(new Date());
        comment.setDiscussId(discussId);
        comment.setLevel(1);
        comment.setCreateTime(new Date());
        comment.setCommentContext(replyContext);
        comment.setCommentStatus(1);
        return commentMapper.insert(comment) >0;
    }

    @Override
    public boolean insertReplyCommentSolution(Long discussId, Integer parentId, Long userId, String commentText) {
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setUpdateTime(new Date());
        comment.setDiscussId(discussId);
        comment.setCreateTime(new Date());
        comment.setCommentContext(commentText);
        comment.setParentId(parentId);
        comment.setCommentStatus(1);
        return commentMapper.insert(comment) >0;
    }
}
