package cn.wen.service.impl;

import cn.wen.dao.ResultDao;
import cn.wen.entity.Result;
import cn.wen.mapper.ResultMapper;
import cn.wen.service.ResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class ResultServiceImpl extends ServiceImpl<ResultMapper, Result> implements ResultService {

    @Resource
    private ResultMapper resultMapper;

    @Resource
    private ResultDao resultDao;

    @Override
    public boolean insertResult(Long userId, Long subjectId,String resultContext) {
        Result result = new Result();
        result.setCreateTime(new Date());
        result.setUpdateTime(new Date());
        result.setDeleted(false);
        result.setResultContext(resultContext);
        result.setResultStatus(1);
        int insert = resultMapper.insert(result);
        if (insert <0){
            return false;
        }
        // 插入关系
        return resultDao.insertResultSubjectRelation(subjectId, userId, result.getId());
    }
}
