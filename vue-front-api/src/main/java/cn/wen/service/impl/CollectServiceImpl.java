package cn.wen.service.impl;

import cn.wen.dao.CollectDao;
import cn.wen.entity.Collect;
import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import cn.wen.mapper.CollectMapper;
import cn.wen.service.CollectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class CollectServiceImpl extends ServiceImpl<CollectMapper, Collect> implements CollectService {

    @Resource
    private CollectDao collectDao;

    @Resource
    private CollectMapper collectMapper;

    @Override
    public List<Subject> subjectCollectList(Long userId) {
        return collectDao.subjectCollectByUserId(userId);
    }

    @Override
    public List<Discuss> discussCollectList(Long userId) {
        return collectDao.discussCollectByUserId(userId);
    }

    @Override
    public boolean deleteBySubjectId(Long id,Long userId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("subject_id",id);
        queryWrapper.eq("user_id",userId);
        return collectMapper.delete(queryWrapper) >0;
    }

    @Override
    public boolean deleteByDiscussId(Long id,Long userId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("discuss_id",id);
        queryWrapper.eq("user_id",userId);
        return collectMapper.delete(queryWrapper) >0;
    }

    @Override
    public boolean deleteByDiscussLikeId(Long id, Long userId) {
        return collectDao.deleteByDiscussLikeId(id,userId);
    }

    @Override
    public boolean insertByDiscussLikeId(Long id, Long userId) {
        return collectDao.insertByDiscussLikeId(id,userId);
    }

    @Override
    public boolean insertByDiscussCollectId(Long id, Long userId) {
        Collect collect = new Collect();
        collect.setUserId(userId);
        collect.setDiscussId(id);
        collect.setCreateDate(new Date());
        collect.setUpdateDate(new Date());
        return collectMapper.insert(collect)>0;
    }

}
