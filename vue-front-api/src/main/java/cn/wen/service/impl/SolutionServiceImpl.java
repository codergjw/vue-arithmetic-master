package cn.wen.service.impl;

import cn.wen.dao.SolutionDao;
import cn.wen.dto.SolutionParam;
import cn.wen.dto.SolutionParam;
import cn.wen.entity.Comment;
import cn.wen.entity.Solution;
import cn.wen.entity.Subject;
import cn.wen.entity.User;
import cn.wen.mapper.CommentMapper;
import cn.wen.mapper.SolutionMapper;
import cn.wen.mapper.UserMapper;
import cn.wen.service.SolutionService;
import cn.wen.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class SolutionServiceImpl extends ServiceImpl<SolutionMapper, Solution> implements SolutionService {


    @Resource
    private CommentMapper commentMapper;

    @Resource
    private SolutionMapper solutionMapper;

    @Resource
    private UserMapper userMapper;

    @Resource
    private SolutionDao solutionDao;

    @Override
    public SolutionParam getSolutionById(Long solutionId, Long userId) {
        SolutionParam solutionParam = new SolutionParam();
        Solution solution = solutionMapper.selectById(solutionId);
        BeanUtils.copyProperties(solution,solutionParam);
        User user = userMapper.selectById(userId);
        solutionParam.setNickname(user.getNickname());
        solutionParam.setUserAvatar(user.getAvatar());
        return solutionParam;
    }

    @Override
    public SolutionParam getActiveSolutionIdById(Long subjectId) {
        return solutionDao.getActiveSolutionIdById(subjectId);
    }

    @Override
    public boolean insertCommentSolution(Long solutionId,Long userId, String commentText) {
        Comment comment = new Comment();
        comment.setUserId(userId);
        comment.setUpdateTime(new Date());
        comment.setSolutionId(solutionId);
        comment.setLevel(1);
        comment.setCreateTime(new Date());
        comment.setCommentContext(commentText);
        comment.setCommentStatus(1);
        return commentMapper.insert(comment) >0;
    }
}
