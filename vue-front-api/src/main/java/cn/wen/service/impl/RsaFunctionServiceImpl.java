package cn.wen.service.impl;

import cn.wen.dao.RsaFunctionDao;
import cn.wen.entity.RsaFunction;
import cn.wen.entity.RsaLinear;
import cn.wen.mapper.RsaFunctionMapper;
import cn.wen.mapper.RsaLinearMapper;
import cn.wen.service.RsaFunctionService;
import cn.wen.utils.StringCompiler;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class RsaFunctionServiceImpl extends ServiceImpl<RsaFunctionMapper, RsaFunction> implements RsaFunctionService {

    @Resource
    private RsaFunctionMapper rsaFunctionMapper;

    @Resource
    private RsaFunctionDao rsaFunctionDao;

    @Resource
    private RsaLinearMapper rsaLinearMapper;

    @Override
    public List<RsaFunction> rsaFunctionList(String keyword) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.like("rsa_name",keyword);
        return rsaFunctionMapper.selectList(queryWrapper);
    }

    @Override
    public RsaFunction getRsaFunctionById(Long rsaId) {
        return rsaFunctionMapper.selectById(rsaId);
    }

    @Override
    public double solution(String data,Long rsaId) {
        /**
         * 获取当前活跃算法
         */
        RsaLinear linearByRsaId = rsaFunctionDao.getLinearByRsaId(rsaId);
        System.out.println(linearByRsaId.getLinearContext());
        System.out.println(data);
        String [] strings = data.split(";");
        int i =0,j =0,n =4;
        double arr [][] = new double[n][n];
        for (String string : strings) {
            String[] split = string.split(",");
            for (String s : split) {
                System.out.print(s+"、");
                arr[j][i++] = Double.parseDouble(s);
            }
            j++;
            i = 0;
        }
        for (double[] doubles : arr) {
            for (double aDouble : doubles) {
                System.out.print(aDouble);
            }
            System.out.println();
        }
        double run = 0;
        try {
            run = (double) StringCompiler.run(linearByRsaId.getLinearContext(), arr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return run;
    }
}
