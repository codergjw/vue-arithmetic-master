package cn.wen.service.impl;

import cn.wen.dao.CommentDao;
import cn.wen.dto.CommentParam;
import cn.wen.entity.Comment;
import cn.wen.mapper.CommentMapper;
import cn.wen.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Resource
    private CommentDao commentDao;

    @Resource
    private CommentMapper commentMapper;

    @Override
    public List<CommentParam> list(Long discussId,Long userId) {
        List<CommentParam> commentList = commentDao.commentList(discussId);
        //获取二级评论
        for (CommentParam commentParam : commentList) {
            commentParam.setInOpen(false);
            commentParam.setLikeNumber(commentDao.getLikeNumberById(commentParam.getId()));
            commentParam.setMyUp(commentDao.getIsLikeById(userId,commentParam.getId()) == null ? 0 : commentDao.getIsLikeById(userId,commentParam.getId()));
            commentParam.setReplyComments(commentDao.listTreeComment(commentParam.getId()));
        }
        List<CommentParam> commentParams = eachComment(commentList,userId);
        return commentParams;
    }

    @Override
    public List<CommentParam> listSolutionComment(Long solutionId, Long userId) {
        List<CommentParam> commentSolutionList = commentDao.commentSolutionList(solutionId);
        //获取二级评论
        for (CommentParam commentParam : commentSolutionList) {
            commentParam.setInOpen(false);
            commentParam.setLikeNumber(commentDao.getLikeNumberById(commentParam.getId()));
            commentParam.setMyUp(commentDao.getIsLikeById(userId,commentParam.getId()) == null ? 0 : commentDao.getIsLikeById(userId,commentParam.getId()));
            commentParam.setReplyComments(commentDao.listTreeComment(commentParam.getId()));
        }
        List<CommentParam> commentParams = eachComment(commentSolutionList,userId);
        return commentParams;
    }

    /**
     * 循环每个顶级的评论节点
     *
     * @param comments
     * @return
     */
    private List<CommentParam> eachComment(List<CommentParam> comments,Long userId) {
        List<CommentParam> commentsView = new ArrayList<>();
        for (CommentParam comment : comments) {
            CommentParam c = new CommentParam();
            BeanUtils.copyProperties(comment, c);
            commentsView.add(c);
        }
        //合并评论的各层子代到第一级子代集合中
        combineChildren(commentsView,userId);
        return commentsView;
    }

    /**
     * @param comments root根节点，blog不为空的对象集合
     * @return
     */
    private void combineChildren(List<CommentParam> comments, Long userId) {

        for (CommentParam comment : comments) {
            List<CommentParam> replys1 = comment.getReplyComments();
            for (CommentParam reply1 : replys1) {
                //循环迭代，找出子代，存放在tempReplys中
                recursively(reply1, userId);
            }
            //修改顶级节点的reply集合为迭代处理后的集合
            comment.setReplyComments(tempReplys);
            //清除临时存放区
            tempReplys = new ArrayList<>();
        }
    }

    //存放迭代找出的所有子代的集合
    private List<CommentParam> tempReplys = new ArrayList<>();

    /**
     * 递归迭代，剥洋葱
     *
     * @param comment 被迭代的对象
     * @return
     */
    private void recursively(CommentParam comment,Long userId) {

        comment.setLikeNumber(commentDao.getLikeNumberById(comment.getId()));
        comment.setMyUp(commentDao.getIsLikeById(userId,comment.getId()) == null ? 0 : commentDao.getIsLikeById(userId,comment.getId()));
        tempReplys.add(comment);//顶节点添加到临时存放集合
        if (comment.getReplyComments().size() > 0) {
            List<CommentParam> replys = comment.getReplyComments();
            for (CommentParam reply : replys) {
                CommentParam commentParam = new CommentParam();
                BeanUtils.copyProperties(reply,commentParam);
                commentParam.setLikeNumber(commentDao.getLikeNumberById(commentParam.getId()));
                commentParam.setMyUp(commentDao.getIsLikeById(userId,commentParam.getId()) == null ? 0 : commentDao.getIsLikeById(userId,commentParam.getId()));
                tempReplys.add(commentParam);
                if (reply.getReplyComments().size() > 0) {
                    recursively(reply,userId);
                }
            }
        }
    }


    @Override
    public boolean updateComment(Comment comment) {
        return commentDao.updateComment(comment);
    }

    @Override
    public boolean deleteComment(Long id) {
        return commentMapper.deleteById(id) >0;
    }
}
