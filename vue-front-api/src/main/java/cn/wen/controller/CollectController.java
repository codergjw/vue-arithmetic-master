package cn.wen.controller;


import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import cn.wen.response.Result;
import cn.wen.service.CollectService;
import com.alibaba.fastjson.JSON;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web")
public class CollectController {

    @Resource
    private CollectService collectService;

    /**
     * 通过userId来获取收藏列表
     */
    @GetMapping("/collect/collect-list/{id}")
    public String list(@PathVariable("id") Long userId) {
        List<Subject> subjects = collectService.subjectCollectList(userId);
        return JSON.toJSONString(new Result().setData(subjects).setCode(200));
    }

    /**
     * 通过userId来获取收藏列表
     */
    @GetMapping("/collect/discuss-list/{id}")
    public String discussList(@PathVariable("id") Long userId) {
        List<Discuss> discusses = collectService.discussCollectList(userId);
        return JSON.toJSONString(new Result().setData(discusses).setCode(200));
    }

    /**
     * 通过subjectId来删除收藏列表
     */
    @PostMapping("/collect/delete-subject/{id}/{userId}")
    public String deleteSubject(@PathVariable("id") Long subjectId,@PathVariable("userId") Long userId) {
        boolean delete = collectService.deleteBySubjectId(subjectId,userId);
        if (!delete){
            return JSON.toJSONString(new Result().setCode(500));
        }
        return JSON.toJSONString(new Result().setCode(200));
    }

    /**
     * 通过userId来获取收藏列表
     */
    @PostMapping("/collect/delete-discuss/{id}/{userId}")
    public String delete(@PathVariable("id") Long discussId,@PathVariable("userId") Long userId) {
        System.out.println(userId);
        boolean delete = collectService.deleteByDiscussId(discussId,userId);
        if (!delete){
            return JSON.toJSONString(new Result().setCode(500));
        }
        return JSON.toJSONString(new Result().setCode(200));
    }

    /**
     * 通过id userId来删除收藏
     */

}
