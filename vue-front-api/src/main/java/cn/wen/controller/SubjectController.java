package cn.wen.controller;


import cn.wen.dao.SubjectDao;
import cn.wen.dto.SubjectParam;
import cn.wen.entity.Collect;
import cn.wen.entity.RsaFunction;
import cn.wen.entity.Subject;
import cn.wen.mapper.CollectMapper;
import cn.wen.mapper.SubjectMapper;
import cn.wen.response.Result;
import cn.wen.service.SubjectService;
import com.alibaba.fastjson.JSON;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

import java.util.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web/subject")
public class SubjectController {

    @Resource
    private SubjectService subjectService;

    @Resource
    private SubjectMapper subjectMapper;

    @Resource
    private SubjectDao subjectDao;

    @Resource
    private CollectMapper collectMapper;

    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword,
                       @RequestParam(value = "subjectClass", required = false) int subjectClass) {
        List<Subject> subjectList = subjectService.subjectList(keyword, subjectClass);
        return JSON.toJSONString(new Result().setData(subjectList).setCode(200));
    }

    /**
     * 获取题目详情
     */
    @GetMapping("/getInfo/{id}/{userId}")
    public String getInfo(@PathVariable("id") Long subjectId, @PathVariable("userId") Long userId) {
        SubjectParam subjectById = subjectService.getSubjectById(subjectId,userId);
        return JSON.toJSONString(new Result().setData(subjectById).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/up/{id}/{userId}")
    public String up(@PathVariable("id") Long subjectId,@PathVariable("userId") Long userId) {
        boolean b = subjectDao.insertUpById(userId, subjectId);
        return JSON.toJSONString(new Result().setData(b).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/down/{id}/{userId}")
    public String down(@PathVariable("id") Long subjectId,@PathVariable("userId") Long userId) {
        boolean b = subjectDao.deleteUpById(userId, subjectId);
        return JSON.toJSONString(new Result().setData(b).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/insert-collect/{id}/{userId}")
    public String insertCollect(@PathVariable("id") Long subjectId,@PathVariable("userId") Long userId) {
        Collect entity = new Collect();
        entity.setUserId(userId);
        entity.setSubjectId(subjectId);
        entity.setCreateDate(new Date());
        entity.setUpdateDate(new Date());
        int insert = collectMapper.insert(entity);
        return JSON.toJSONString(new Result().setData(insert).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/delete-collect/{id}/{userId}")
    public String deleteCollect(@PathVariable("id") Long subjectId,@PathVariable("userId") Long userId) {
        boolean b = subjectDao.deleteCollectById(userId, subjectId);
        return JSON.toJSONString(new Result().setData(b).setCode(200));
    }

}
