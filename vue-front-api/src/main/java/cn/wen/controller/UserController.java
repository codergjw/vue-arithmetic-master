package cn.wen.controller;


import cn.wen.entity.User;
import cn.wen.response.Result;
import cn.wen.service.UserService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web/user")
public class UserController {

    @Resource
    private UserService userService;

    @Value("${user.region}")
    private String region;

    @Value("${user.accessKeyId}")
    private String accessKeyId;

    @Value("${user.accessKeySecret}")
    private String accessKeySecret;

    @Value("${user.bucket}")
    private String bucket;

    /**
     * 获取用户信息
     */
    @ApiOperation("获取用户信息")
    @GetMapping("/info")
    public String getAdminInfo(@RequestParam String username){
        if(username == null || username.equals("")){
            return JSON.toJSONString(new Result().setCode(500).setMessage("信息获取失败！"));
        }
        User resultUser = userService.getUserByUsername(username);
        Map<Object, Object> map = new HashMap<>();
        map.put("username", resultUser.getUsername());
        map.put("icon", resultUser.getAvatar());
        return JSON.toJSONString(new Result().setCode(200).setMessage("登录成功").setData(map));
    }


    /**
     * 修改信息
     */
    @ApiOperation("修改用户信息")
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable Long id,@RequestBody User user) {
        System.out.println(user);
        user.setId(id);
        boolean result = userService.updateUsrById(user);
        if (result) {
            User userById = userService.getUserById(id);
            return JSON.toJSONString(new Result().setData(userById).setCode(200).setMessage("修改成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("修改失败"));
    }

    /**
     * 获取用户的oss信息
     *
     */
    @GetMapping("/aliyun/oss/policy")
    public String OssPolicy(){
        Map<String, String> oss = new HashMap<>();
        oss.put("region",region);
        oss.put("accessKeyId",accessKeyId);
        oss.put("accessKeySecret",accessKeySecret);
        oss.put("bucket",bucket);
        return JSON.toJSONString(new Result().setCode(200).setData(oss));
    }

}
