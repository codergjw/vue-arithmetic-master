package cn.wen.controller;


import cn.wen.entity.RsaFunction;
import cn.wen.entity.Subject;
import cn.wen.response.Result;
import cn.wen.service.RsaFunctionService;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web/rsa-function")
public class RsaFunctionController {

    @Resource
    private RsaFunctionService rsaFunctionService;

    @GetMapping("/list")
    public String list(@RequestParam(value = "keyword", required = false) String keyword) {
        List<RsaFunction> rsaFunctionList = rsaFunctionService.rsaFunctionList(keyword);
        return JSON.toJSONString(new Result().setData(rsaFunctionList).setCode(200));
    }

    @GetMapping("/getInfo/{id}")
    public String getInfo(@PathVariable("id") Long rsaId) {
        RsaFunction rsaFunctionById = rsaFunctionService.getRsaFunctionById(rsaId);
        return JSON.toJSONString(new Result().setData(rsaFunctionById).setCode(200));
    }

    @GetMapping("/solution/{id}")
    public String solution(@PathVariable("id") Long rsaId,
                           @RequestParam(value = "returnData") String data) {
        double solution = rsaFunctionService.solution(data, rsaId);
        return JSON.toJSONString(new Result().setData(solution).setCode(200));
    }

}
