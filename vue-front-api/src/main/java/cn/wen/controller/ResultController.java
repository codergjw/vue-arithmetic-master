package cn.wen.controller;


import cn.wen.response.Result;
import cn.wen.service.ResultService;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web/result")
public class ResultController {

    @Resource
    private ResultService resultService;

    @PostMapping("/insert/{id}")
    public String insertResult(@PathVariable("id") Long userId,
                               @RequestParam("subjectId") String subjectId,
                               @RequestParam("resultContext") String resultContext){
        boolean b = resultService.insertResult(userId, Long.parseLong(subjectId), resultContext);
        if (b){
            return JSON.toJSONString(new Result().setCode(200).setMessage("添加成功!!"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("添加失败！!"));
    }

}
