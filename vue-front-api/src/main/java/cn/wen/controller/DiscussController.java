package cn.wen.controller;

import cn.wen.common.response.CommonResult;
import cn.wen.dao.DiscussDao;
import cn.wen.dto.DiscussFrontParam;
import cn.wen.entity.Collect;
import cn.wen.entity.Discuss;
import cn.wen.entity.Subject;
import cn.wen.mapper.CollectMapper;
import cn.wen.response.Result;
import cn.wen.service.CommentService;
import cn.wen.service.DiscussService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web/discuss")
public class DiscussController {

    @Resource
    private DiscussDao discussDao;

    @Resource
    private DiscussService discussService;

    @Resource
    private CollectMapper collectMapper;

    @Resource
    private CommentService commentService;

    @GetMapping("/list/{id}/{userId}")
    public String list(@PathVariable("id") Integer sort, @PathVariable("userId") Long userId) {
        List<DiscussFrontParam> list = discussService.list(sort, userId);
        return JSON.toJSONString(new Result().setData(list).setCode(200));
    }

    /**
     * 注册功能
     */
    @PostMapping("/create/{id}")
    @ApiOperation("添加讨论信息")
    public String discussCreate(@RequestBody Discuss createDiscuss, @PathVariable("id") Long userId) {
        boolean resultSubject = discussService.createDiscuss(createDiscuss, userId);
        if (!resultSubject) {
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("创建失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("创建成功！！"));

    }

    @GetMapping("/getInfo/{id}/{userId}")
    public String list(@PathVariable("id") Long discussId, @PathVariable("userId") Long userId) {
        DiscussFrontParam discussFrontParam = discussService.getDiscussById(userId, discussId);
        Map<String, Object> map = new HashMap<>();
        map.put("discuss", discussFrontParam);
        map.put("comment", commentService.list(discussId, userId));
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 回复讨论
     */
    @PostMapping("/reply/{id}")
    public String discussReply(@PathVariable("id") Long userId,
                               @RequestParam("discussId") Long discussId,
                               @RequestParam("parentId") Long parentId,
                               @RequestParam("replyDiscussContext") String replyDiscussContext) {
        boolean result = false;
        if (parentId == 0) {
            result = discussService.insertReplyDiscuss(discussId, userId, replyDiscussContext);
        } else {
            result = discussService.insertReplyCommentSolution(discussId, Long.valueOf(parentId).intValue(), userId, replyDiscussContext);
        }
        if (!result) {
            return JSON.toJSONString(new CommonResult().setCode(500).setMessage("回复失败！！"));
        }
        return JSON.toJSONString(new CommonResult().setCode(200).setMessage("回复成功！！"));

    }

    /**
     * 点赞
     */
    @PostMapping("/up/{id}/{userId}")
    public String up(@PathVariable("id") Long discussId, @PathVariable("userId") Long userId) {
        boolean b = discussDao.insertUpById(userId, discussId);
        return JSON.toJSONString(new Result().setData(b).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/down/{id}/{userId}")
    public String down(@PathVariable("id") Long discussId, @PathVariable("userId") Long userId) {
        boolean b = discussDao.deleteUpById(userId, discussId);
        return JSON.toJSONString(new Result().setData(b).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/insert-collect/{id}/{userId}")
    public String insertCollect(@PathVariable("id") Long discussId, @PathVariable("userId") Long userId) {
        Collect entity = new Collect();
        entity.setUserId(userId);
        entity.setDiscussId(discussId);
        entity.setCreateDate(new Date());
        entity.setUpdateDate(new Date());
        int insert = collectMapper.insert(entity);
        return JSON.toJSONString(new Result().setData(insert).setCode(200));
    }

    /**
     * 点赞
     */
    @PostMapping("/delete-collect/{id}/{userId}")
    public String deleteCollect(@PathVariable("id") Long discussId, @PathVariable("userId") Long userId) {
        boolean b = discussDao.deleteCollectById(userId, discussId);
        return JSON.toJSONString(new Result().setData(b).setCode(200));
    }

    /**
     * 获取相关列表
     */
    @GetMapping("/related/list")
    public String relatedList(@RequestParam(value = "discussTag", required = false) String discussTag,
                              @RequestParam(value = "discussTitle", required = false) String discussTitle) {
        List<DiscussFrontParam> list = discussService.list(discussTag, discussTitle);
        return JSON.toJSONString(new Result().setData(list).setCode(200));
    }

    @PostMapping("/delete-comment/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = commentService.deleteComment(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

}
