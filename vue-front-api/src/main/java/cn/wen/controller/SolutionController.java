package cn.wen.controller;


import cn.wen.dto.DiscussFrontParam;
import cn.wen.dto.SolutionParam;
import cn.wen.dto.SubjectParam;
import cn.wen.response.Result;
import cn.wen.service.CommentService;
import cn.wen.service.SolutionService;
import cn.wen.service.SubjectService;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author FY
 * @since 2022-02-13
 */
@RestController
@RequestMapping("/web/solution")
public class SolutionController {


    @Resource
    private SolutionService solutionService;

    @Resource
    private CommentService commentService;

    /**
     * 获取题目详情
     */
    @GetMapping("/getInfo/{id}/{userId}")
    public String getInfo(@PathVariable("id") Long subjectId, @PathVariable("userId") Long userId) {
        // 先获取subjectId 的开启的solution 的id
        SolutionParam solutionById = solutionService.getSolutionById(subjectId, userId);
        Map<String, Object> map = new HashMap<>();
        map.put("solution",solutionById);
        map.put("comment",commentService.listSolutionComment(solutionById.getId(),userId));
        return JSON.toJSONString(new Result().setData(map).setCode(200));
    }

    /**
     * 添加评论
     */
    @PostMapping("/insert-comment/{id}/{userId}")
    public String insertComment(@PathVariable("id") Long solutionId,
                                @PathVariable("userId") Long userId,
                                @RequestParam("commentText") String commentText){
        boolean b = solutionService.insertCommentSolution(solutionId, userId, commentText);
        if (b){
            return JSON.toJSONString(new Result().setData(null).setCode(200));
        }
        return JSON.toJSONString(new Result().setData(null).setCode(500));
    }

    @PostMapping("/delete-comment/{id}")
    public String delete(@PathVariable Long id) {
        boolean count = commentService.deleteComment(id);
        if (count) {
            return JSON.toJSONString(new Result().setCode(200).setMessage("删除成功"));
        }
        return JSON.toJSONString(new Result().setCode(500).setMessage("删除失败"));
    }

}
