package cn.wen.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.DigestUtils;

import java.util.Random;
import java.util.UUID;


/**
 * @author 何进业
 */
public class MD5Util {
    /**
     * 生成随机字符串
     */
    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    /**
     * MD5加密
     */
    public static String md5(String key){
        if (StringUtils.isBlank(key)){
            return null;
        }
        return DigestUtils.md5DigestAsHex(key.getBytes());
    }

    public static Integer changeString(String string){
        if (string != null && !StringUtils.isBlank(string)) {
            return Integer.valueOf(string);
        }else {
            return null;
        }
    }

    //随机生成salt
    public static String saltUtil(){
        String goalStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz*+-.%";
        StringBuilder saltStr = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            char c = goalStr.charAt(new Random().nextInt(goalStr.length()));
            saltStr.append(c);
        }
        return new String(saltStr);
    }

    public static void main(String[] args) {

        System.out.println(md5("123456+AFZrI"));

    }
}
